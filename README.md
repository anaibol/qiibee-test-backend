# Backend

On the backend, I've been learning a lot about how a GraphQL server works. I started the setup of a GraphQL served based on Prisma and created a model for users and transactions.
Having used Graphcool I already have some backend GraphQL experience, but Prisma is more "low level" than Graphcool.

- It's not a Backend As A Service, but a building block of your backend, between your GraphQL server (GraphQL Yoga, for example) and your database (only MySQL and Postgres supported at the moment), sort of ORM.
- It generates many methods to interact with the database based on your datamodel.
- It comes with a powerful GraphQL IDE and a console to interact directly with your data.
- It takes care of migrations of your data model. As on Graphcool, you can specify directives on your GraphQL types to describe how the new model will transform the old data on the database.

## Get started

1.  Clone
2.  Run  `yarn`
3.  Run  `yarn start`

The server is now running on [http://localhost:4000](http://localhost:4000).

### Open a Playground
You can open a Playground by navigating to [http://localhost:4000](http://localhost:4000) in your browser.

At the right side of the playground you can find the green "SCHEMA" button at the top right to see the different possible queries, mutations and the fields required. 

![](https://i.imgur.com/KvUmomP.png)

### Register a new user with the `signup` mutation

You can send the following mutation in the Playground to create a new `User` node and at the same time retrieve an authentication token for it:

```graphql
mutation {
  signup(email: "hey@federi.co" password: "mypassword") {
    token
  }
}
```


![](https://i.imgur.com/mlSuTKt.png)

### Logging in an existing user with the `login` mutation

This mutation will log in an existing user by requesting a new authentication token for her:

```graphql
mutation {
  login(email: "hey@federi.co" password: "mypassword") {https://i.imgur.com/KvUmomP.pnghttps://i.imgur.com/KvUmomP.pnghttps://i.imgur.com/KvUmomP.png
    token
  }
}
```

![](https://i.imgur.com/1QVHfZ8.png)


### Checking whether a user is currently logged in with the `me` query

For this query, you need to make sure a valid authentication token is sent along with the `Bearer `-prefix in the `Authorization` header of the request. Inside the Playground, you can set HTTP headers in the bottom-left corner:


![](https://i.imgur.com/mlSuTKt.png)

Once you've set the header, you can send the following query to check whether the token is valid:

```graphql
{
  me {
    id
    email
  }
}
```

If the token is valid, the server will return the `id` and `email` of the `User` node that it belongs to.
